import {
    NONE,
    ERROR,
    WARN,
    INFO,
    TRACE
} from './Logger';

export const logLevel = TRACE;
// export const logLevel = NONE;

export let FRONTEND_BASE_URL;
if (process.env.NODE_ENV === 'development') {
    FRONTEND_BASE_URL = 'localhost:3000';
} else {
    FRONTEND_BASE_URL = 'beningo.djvk.net';
}

export const TYPE_BENIOFF = 'benioff';
export const TYPE_BLITZER = 'blitzer';

export const HOSTNAME_MAPPINGS = {
    localhost: TYPE_BENIOFF,
    'beningo.djvk.net': TYPE_BENIOFF,
    'benibingo.com': TYPE_BENIOFF,
    'blitzerbingo.com': TYPE_BLITZER,
};

export const TYPES = [
    TYPE_BENIOFF,
    TYPE_BLITZER
];

export const getBoardType = (queryParams) => {
    let hostname = window.location.hostname;
    if (HOSTNAME_MAPPINGS.hasOwnProperty(hostname)) {
        return HOSTNAME_MAPPINGS[hostname];
    }
    let typeParam = queryParams['type'] || null;
    if (!typeParam || TYPES.indexOf(typeParam) === -1) {
        return TYPE_BENIOFF;
    }
    else return typeParam;
};

export const getCenterImageName = (boardType) => {
    switch (boardType) {
        case TYPE_BENIOFF:
            return '/benioff.png';
        case TYPE_BLITZER:
            return '/blitzer.jpeg';
        default:
            throw new Error(`Invalid board type ${boardType}`);
    }
};
