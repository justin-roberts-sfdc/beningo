/**
 * @callback objectKeyCallback
 * @param {Object} currentValue
 */

/**
 * @param {Object[]} input
 * @param {objectKeyCallback} callback
 * @return {Object}
 */
export function objectKeysByCallback(input, callback) {
    return input.reduce((carry, current) => {
        carry[callback(current)] = current;
        return carry;
    }, {});
}

/**
 * @param {Object} input
 * @return {Object}
 */
export function arrayFlip(input) {
    let out = {};
    for(let [key, value] of Object.entries(input)) {
        out[value] = key;
    }
    return out;
}

export function getCenterCoordinates(boardCellsX, boardCellsY) {
    return [Math.floor(boardCellsX / 2), Math.floor(boardCellsY / 2)];
}

