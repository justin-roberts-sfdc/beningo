import { connect } from 'react-redux';
import {
    toggleSquare,
    init,
} from '../modules/board';
import sizeMe from 'react-sizeme';
import { createSelector } from 'reselect';
import Board from '../components/Board';

const mapDispatchToProps = {
    toggleSquare: toggleSquare,
    init,
};

const hasInitialized = (boardState) => {
    return boardState.selectedCells.length > 0 &&
        Object.keys(boardState.boardSize).length === 2 &&
        boardState.wordPool.length > 0;
};

const truthyMergeNestedArrays = (...grids) => {
    if (!grids) {
        return [];
    }
    let out = grids.pop();
    grids.forEach((grid) => {
        grid.forEach((row, y) => {
            row.forEach((cell, x) => {
                out[y][x] = out[y][x] || cell;
            });
        });
    });
    return out;
};

// Selectors
const horizontalBingos = (state) => {
    if (!hasInitialized(state.board)) {
        return [];
    }
    let {
        boardSize: {
            x: boardCellsX,
            y: boardCellsY,
        },
        selectedCells,
    } = state.board;
    // Iterate over each row
    return [...Array(boardCellsY)].map((_, y) => {
        // For each row, check if all columns are selected
        let hasBingoed = (selectedCells[y].every((cellSelected) => cellSelected));
        return [...Array(boardCellsX)].map(() => hasBingoed);
    });
};

const verticalBingos = (state) => {
    if (!hasInitialized(state.board)) {
        return [];
    }
    let {
        boardSize: {
            x: boardCellsX,
            y: boardCellsY,
        },
        selectedCells,
    } = state.board;
    let bingos = [...Array(boardCellsY)].map(() => {
        return [...Array(boardCellsX)].map(() => false);
    });
    // Iterate over each column
    [...Array(boardCellsX)].forEach((_, x) => {
        // For each column, check if all rows are selected
        if ([...Array(boardCellsY)].every((_, y) => selectedCells[y][x])) {
            [...Array(boardCellsY)].forEach((_, y) => bingos[y][x] = true);
        }
    });
    return bingos;
};

const diagonalBingos = (state) => {
    if (!hasInitialized(state.board)) {
        return [];
    }
    let {
        boardSize: {
            x: boardCellsX,
            y: boardCellsY,
        },
        selectedCells,
    } = state.board;

    let bingos = [...Array(boardCellsY)].map(() => {
        return [...Array(boardCellsX)].map(() => false);
    });
    let leftDiagonal = [];
    let rightDiagonal = [];

    [...Array(boardCellsX)].forEach((_, x) => {
        let directY = x;
        let inverseY = boardCellsY - 1 - directY;
        leftDiagonal.push([x, directY]);
        rightDiagonal.push([x, inverseY]);
    });

    [leftDiagonal, rightDiagonal].forEach((diagonal) => {
        if (diagonal.every(([x, y]) => selectedCells[y][x])) {
            diagonal.forEach(([x, y]) => bingos[y][x] = true);
        }
    });
    return bingos;
};

const bingoSelector = createSelector([horizontalBingos, verticalBingos, diagonalBingos],
    (horizontalBingos, verticalBingos, diagonalBingos) => {
        return truthyMergeNestedArrays(horizontalBingos, verticalBingos, diagonalBingos);
    }
);

const hasBingoedSelector = createSelector([bingoSelector],
    (bingoSelector) => {
        return bingoSelector.some((row) => {
            return row.some((cell) => cell);
        });
    }
);

const mapStateToProps = (state) => {
    return {
        ...state.board,
        bingos: bingoSelector(state),
        hasBingoed: hasBingoedSelector(state),
    };
};

const sizeMeConfig = {
    monitorHeight: true,
    monitorWidth: true,
};

export default sizeMe(sizeMeConfig)(connect(mapStateToProps, mapDispatchToProps)(Board));
