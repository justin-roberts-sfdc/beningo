import {
    all,
    call,
    fork,
    spawn,
    put,
    take,
    takeLatest,
    cancelled,
    race,
} from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import {
    delay,
} from 'redux-saga';
import { objectKeysByCallback } from '../../../lib/GeneralUtils';
import {
    log,
    ERROR,
    TRACE,
} from '../../../lib/Logger';
import {
    displayModal,
    MODAL_TYPE_ERROR,
} from '../../../store/globalUi';
import {
} from '../modules/board';

/**
 * Root saga started from createStore
 */
export function* rootBoardSaga(dispatch) {
    yield all([
        // spawn(watchDecideStart, dispatch),
    ]);
}
