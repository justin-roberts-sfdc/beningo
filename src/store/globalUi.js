
// ------------------------------------
// Constants
// ------------------------------------
export const MODAL_TYPE_ERROR = 'MODAL_TYPE_ERROR';
export const MODAL_TYPE_WARNING = 'MODAL_TYPE_WARNING';
export const MODAL_TYPE_INFO = 'MODAL_TYPE_INFO';
export const MODAL_TYPE_SUCCESS = 'MODAL_TYPE_SUCCESS';

const DISPLAY_MODAL = 'DISPLAY_MODAL';
const DISMISS_MODAL = 'DISMISS_MODAL';

// ------------------------------------
// Actions
// ------------------------------------
export function displayModal(message = '', messageType) {
    return {
        type: DISPLAY_MODAL,
        message,
        messageType
    };
}

export function dismissModal() {
    return {
        type: DISMISS_MODAL,
    };
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------
// export const updateLocation = ({ dispatch }) => {
//     return (nextLocation) => dispatch(locationChange(nextLocation));
// };

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    modalMessage: '',
    modalMessageType: null,
};
export default function globalUiReducer (state = initialState, action) {
    switch (action.type) {
        case DISPLAY_MODAL:
            return {
                ...state,
                modalMessage: action.message,
                modalMessageType: action.type,
            };
        case DISMISS_MODAL:
            return initialState;
    }
    return state;
}
